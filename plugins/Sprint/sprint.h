#ifndef SPRINT_H
#define SPRINT_H

#include <QObject>

class Sprint: public QObject {
    Q_OBJECT

public:
    Sprint();
    ~Sprint() = default;
};

#endif
