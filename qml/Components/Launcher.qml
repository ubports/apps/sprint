import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import Sprint 1.0

MouseArea {
    property var app
    property alias color: label.color
    property bool launchApp: true

    signal launch()

    propagateComposedEvents: true
    onClicked: {
        launch();

        if (launchApp && app && app.id) {
            Qt.openUrlExternally(app.uri);
        }
    }

    UbuntuShape {
        id: ubuntuShapeIcon
        visible: !!(app && app.id)

        anchors {
            fill: parent
            topMargin: units.gu(1)
            leftMargin: units.gu(1.2)
            rightMargin: units.gu(1.2)
            bottomMargin: units.gu(4)
        }

        radius: 'medium'
        aspect: UbuntuShape.DropShadow

        image: Image {
            id: icon

            source: {
                if (!app || !app.id) {
                    return '';
                }
                else if (IconPacks.hasCurrentIconPack) {
                    var icon = IconPacks.currentIconPack.getIcon(app.id);
                    if (icon) {
                        return icon;
                    }
                }

                return app.icon;
            }
            asynchronous: true

            fillMode: Image.PreserveAspectFit
            sourceSize {
                width: ubuntuShapeIcon.width
                height: ubuntuShapeIcon.height
            }
        }
    }

    Label {
        id: label

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        height: units.gu(4)

        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        text: (app && app.id) ? app.name : ''
    }
}
