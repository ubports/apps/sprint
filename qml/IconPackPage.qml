import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Page {
    property var iconPack
    property var settings

    header: PageHeader {
        id: header
        title: iconPack.title
    }

    ColumnLayout {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }

        spacing: units.gu(1)

        Image {
            Layout.fillWidth: true

            source: iconPack.preview
            fillMode: Image.PreserveAspectFit
        }

        Label {
            text: '<b>' + i18n.tr('Author') + '</b>: ' + iconPack.author

            wrapMode: Text.WordWrap
        }

        Label {
            text: '<b>' + i18n.tr('Maintainer') + '</b>: ' + iconPack.maintainer

            wrapMode: Text.WordWrap
        }

        Label {
            text: iconPack.description

            wrapMode: Text.WordWrap
        }

        Button {
            text: i18n.tr('Switch Icons')
            color: UbuntuColors.orange

            onClicked: {
                if (iconPack.id == 'default') {
                    settings.iconPackId = '';
                }
                else {
                    settings.iconPackId = iconPack.id;
                }

                pageStack.pop();
            }
        }

        Item {
            id: spacer

            Layout.fillHeight: true
        }
    }
}
